﻿using BussinessObject.Models;
using DataAccess.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repo;
using Repo.Interface;
using System.Data;
using System.Text.RegularExpressions;

namespace APIS.Controllers
{
    [Authorize(Roles = "2")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private IStudentRepo repository = new StudentRepo();
        //Get: apu/Products
        [HttpGet]
        public ActionResult<IEnumerable<Student>> GetProducts() => repository.GetStudent();
        [HttpGet("{id}")]
        public ActionResult<Student> GetProductById(int id) => repository.GetStudentById(id);

        [HttpGet("seachbygroupid/{groupid}")]
        public ActionResult<IEnumerable<Student>> GetProductByGroupId(int groupid) => repository.GetStudentByGroupId(groupid);

        [HttpGet("Search/{keyword}")]
        public ActionResult<IEnumerable<Student>> Search(string keyword) => repository.Search(keyword);
        [HttpGet("GetStudentByDate/{keyword}")]
        public ActionResult<IEnumerable<Student>> GetStudentByDate(DateTime keyword) => repository.GetStudentByDate(keyword);
        //Post : ProductsController/Products
        [HttpPost]
        public IActionResult PostStudent(StudentDTO productRequest)
        {
            TimeSpan timeDifference = DateTime.Now - productRequest.DateOfBirth.Value;
            double Age = timeDifference.TotalDays / 365.2425;
            if (Age >= 18 && Age <= 100)
            {
                var p = new Student
                {
                    Id = productRequest.Id,
                    FullName = productRequest.FullName,
                    GroupId = productRequest.GroupId,
                    Email = productRequest.Email,
                    DateOfBirth = productRequest.DateOfBirth
                };
                repository.CreateStudent(p);
               
            }
            else
            {
                return BadRequest(productRequest.DateOfBirth);
            }
            return NoContent();
        }

        //Get: ProductsController/Delete/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var p = repository.GetStudentById(id);
            if (p == null)
                return NotFound();
            repository.DeleteStudent(p);
            return NoContent();
        }
        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, StudentDTO productRequest)
        {
            var pTmp = repository.GetStudentById(id);
            if (pTmp == null)
                return NotFound();
            pTmp.FullName = productRequest.FullName;
            pTmp.GroupId = productRequest.GroupId;
            pTmp.Email = productRequest.Email;
            pTmp.DateOfBirth = productRequest.DateOfBirth;
            repository.UpdateStudent(pTmp);
            return NoContent();
        }

    }
}
