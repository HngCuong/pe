﻿using BussinessObject.Models;
using DataAccess.DTO;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace ViewClient.Controllers
{
    public class StudentController : Controller
    {
        private readonly HttpClient client = null;
        private string ProductApiUrl = "";
        public StudentController()
        {
            client = new HttpClient();
            ProductApiUrl = "https://localhost:7091/api/Student";
        }

        public IActionResult Error()
        {
            return View();
        }
        public async Task<IActionResult> Index()
        {
            try
            {
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                    RequestUri = new Uri(ProductApiUrl)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
                var apiString = await response.Content.ReadAsStringAsync();
                List<Student> listProducts = JsonConvert.DeserializeObject<List<Student>>(apiString);
                Debug.WriteLine(listProducts.Count);
                return View(listProducts);

            }
            catch
            {
                return RedirectToAction("Error", "Student");
            }
        }
        [HttpGet]
        public async Task<IActionResult> Search(int SearchValue)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/seachbygroupid/" + SearchValue)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();

            List<Student> listProducts = JsonConvert.DeserializeObject<List<Student>>(apiString);
            return View(listProducts);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(int id, string email, string name, DateTime birth, int groupid)
        {
            StudentDTO productRequest = new StudentDTO(id, email, name, birth, groupid);
            var json = JsonConvert.SerializeObject(productRequest);
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                Content = new StringContent(json, Encoding.UTF8, "application/json"),
                RequestUri = new Uri(ProductApiUrl)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Error creating product.");
            }
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                    RequestUri = new Uri(ProductApiUrl + "/" + id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);

                if (response.IsSuccessStatusCode)
                {
                    var apiString = await response.Content.ReadAsStringAsync();
                    Student productRequest = JsonConvert.DeserializeObject<Student>(apiString);
                    if (productRequest == null)
                    {
                        return NotFound();
                    }

                    // Pass the productRequest to the view for editing
                    return View(productRequest);
                }
                else
                {
                    // Handle the case where the API request was not successful
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return an error view
                ModelState.AddModelError(string.Empty, "Error loading product for editing.");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, string email, string name, DateTime birth, int groupid)
        {
            try
            {
                StudentDTO productRequest = new StudentDTO(id,email,name,birth,groupid);
                var json = JsonConvert.SerializeObject(productRequest);
                Debug.WriteLine(json);
                var accessToken = HttpContext.Session.GetString("token");
                accessToken = accessToken.Replace("\"", "");
                var requestMessage = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    Content = new StringContent(json, Encoding.UTF8, "application/json"),
                    RequestUri = new Uri(ProductApiUrl + "/" + productRequest.Id)
                };

                requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                var response = await client.SendAsync(requestMessage);
                if (response.IsSuccessStatusCode)
                {
                    // Redirect to the Index action upon successful update
                    return RedirectToAction("Index");
                }
                else
                {
                    // Handle the case where the API request to update the product was not successful
                    ModelState.AddModelError(string.Empty, "Error updating product.");
                }


                // If ModelState is not valid or the API request fails, return to the edit view
                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return to the edit view with an error message
                ModelState.AddModelError(string.Empty, "Error updating product.");
                return RedirectToAction("Edit");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/" + id)
            };

            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return NotFound();
            }
        }
        public async Task<IActionResult> Detail(int id)
        {
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/" +id)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();

            Student listProducts = JsonConvert.DeserializeObject<Student>(apiString);
            return View(listProducts);
        }

        public async Task<IActionResult> SearchDate(DateTime SearchValue)
        {
            string a  = SearchValue.Date.ToString();
            a = a.Replace("/", "-");
            var accessToken = HttpContext.Session.GetString("token");
            accessToken = accessToken.Replace("\"", "");
            var requestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                Content = new StringContent(".....", Encoding.UTF8, "text/plain"),
                RequestUri = new Uri(ProductApiUrl + "/GetStudentByDate/" + a)
            };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await client.SendAsync(requestMessage);
            var apiString = await response.Content.ReadAsStringAsync();

            List<Student> listProducts = JsonConvert.DeserializeObject<List<Student>>(apiString);
            return View(listProducts);
        }
    }
}
