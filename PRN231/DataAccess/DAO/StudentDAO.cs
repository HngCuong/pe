﻿using BussinessObject.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DAO
{
    public class StudentDAO
    {
        public static List<Student> GetProducts()
        {
            var listProducts = new List<Student>();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext()
                )
                {
                    listProducts.AddRange(context.Students);
                };
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return listProducts;
        }
        public static Student FindProductById(int proId)
        {
            Student p = new Student();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    p = context.Students.SingleOrDefault(x => x.Id == proId);
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return p;
        }

        public static List<Student> GetStudentByDate(DateTime keyword)

        {
            var listProducts = new List<Student>();
            var list = new List<Student>();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext()
                )
                {
                    listProducts.AddRange(context.Students);
                };
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            string obj = keyword.Date.ToString();
            if(listProducts is not null)
            {
                foreach(Student op in listProducts)
                { 
                    if (op.DateOfBirth.Value.Date.ToString().Contains(obj))
                    {
                        list.Add(op);
                    }
                }
            }

            return list;
        
        }
        public static List<Student> Search(string keyword)
        {
            var listProduct = new List<Student>();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    listProduct = context.Students.Where(f => f.FullName.Contains(keyword)).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listProduct;
        }
        public static List<Student> FindAllStudentsByGroupId(int groupId)
        {
            var listOrders = new List<Student>();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    listOrders = context.Students.Where(o => o.GroupId == groupId).ToList();
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return listOrders;
        }
        public static void SaveProduct(Student p)
        {
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    context.Students.Add(p);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static void UpdateProduct(Student p)
        {
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    context.Entry<Student>(p).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }
        public static Student DeleteProduct(Student p)
        {
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    var p1 = context.Students.SingleOrDefault(c => c.Id == p.Id);
                    context.Students.Remove(p1);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
            return p;
        }
    }
}
