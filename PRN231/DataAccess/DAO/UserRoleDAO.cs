﻿using BussinessObject.Models;
using DataAccess.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DAO
{
    public class UserRoleDAO
    {
        public static UserRole FindUserByEmail(LoginModel model)
        {
            var customer = new UserRole();
            try
            {
                using (var context = new PRN231_SU23_StudentGroupDBContext())
                {
                    customer = context.UserRoles.FirstOrDefault(c => c.Username == model.Email && c.Passphrase == model.Password);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return customer;
        }
    }
}
