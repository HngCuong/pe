﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DTO
{
    public class StudentDTO
    {
    

        public int Id { get; set; }
        [EmailAddress]
        public string? Email { get; set; }
        [Required(ErrorMessage = "Full Name is required.")]
        [RegularExpression(@"^[A-Z][a-z]*(\s[A-Z][a-z]*)*$", ErrorMessage = "Full Name must start with a capital letter and only contain letters.")]
        public string? FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }
        public int? GroupId { get; set; }
 

        public StudentDTO(int id, string email, string name, DateTime birth, int groupid)
        {
            Id = id;
            Email = email;
            FullName = name;
            DateOfBirth = birth;
            GroupId = groupid;
        }

    }
}
