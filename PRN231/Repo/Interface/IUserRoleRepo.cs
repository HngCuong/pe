﻿using BussinessObject.Models;
using DataAccess.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.Interface
{
    public interface IUserRoleRepo
    {
        UserRole Login(LoginModel model);
    }
}
