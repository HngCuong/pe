﻿using BussinessObject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.Interface
{
    public interface IStudentRepo
    {
        void CreateStudent(Student p);
        Student GetStudentById(int id);
        List<Student> GetStudentByGroupId(int id);
        List<Student> Search(string keyword);
        List<Student> GetStudentByDate(DateTime keyword);
        void UpdateStudent(Student p);
        void DeleteStudent(Student p);
        List<Student> GetStudent();
    }
}
