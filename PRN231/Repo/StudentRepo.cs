﻿using BussinessObject.Models;
using DataAccess.DAO;
using Repo.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public class StudentRepo : IStudentRepo
    {
        public void DeleteStudent(Student p)
      => StudentDAO.DeleteProduct(p);

        public Student GetStudentById(int id)
       => StudentDAO.FindProductById(id);

        public List<Student> GetStudentByGroupId(int id)
       => StudentDAO.FindAllStudentsByGroupId(id);

        public List<Student> GetStudent()
     => StudentDAO.GetProducts();

        public List<Student> Search(string keyword)
      => StudentDAO.Search(keyword);

        public void UpdateStudent(Student p)
       => StudentDAO.UpdateProduct(p);

        public void CreateStudent(Student p)
        {
            StudentDAO.SaveProduct(p);
        }

        public List<Student> GetStudentByDate(DateTime keyword)
        
        =>    StudentDAO.GetStudentByDate(keyword);   
        
    }
}

