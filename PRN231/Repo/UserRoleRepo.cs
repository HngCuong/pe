﻿using BussinessObject.Models;
using DataAccess.DAO;
using DataAccess.DTO;
using Repo.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo
{
    public class UserRoleRepo : IUserRoleRepo
    {
        public UserRole Login(LoginModel model)
        {
            return UserRoleDAO.FindUserByEmail(model);
        }
    }
}
